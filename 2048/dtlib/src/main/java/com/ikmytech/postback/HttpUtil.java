package com.ikmytech.postback;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class HttpUtil {
	public static void http_get(String url, IHttpResponse rsp) {
		new HttpWork(rsp, url).start();
	}

	private static class HttpWork extends Thread {
		private IHttpResponse rsp;
		private String url;

		public HttpWork(IHttpResponse rsp, String url) {
			this.rsp = rsp;
			this.url = url;
		}

		@Override
		public void run() {
			httpGet(url, rsp);
		}

	}

	private static ArrayList<String> httpGet(String urlStr, IHttpResponse rsp) {
		URL url = null;
		HttpURLConnection conn = null;
		InputStreamReader in = null;
		try {
			url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
			int responseCode = conn.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
				rsp.onFailure("");
				return null;
			}
			in = new InputStreamReader(conn.getInputStream());
			BufferedReader bfread = new BufferedReader(in);
			ArrayList<String> result = new ArrayList<String>();
			String readLine = null;
			while ((readLine = bfread.readLine()) != null) {
				result.add(readLine);
			}
			rsp.onResponse(result);
			return result;
		} catch (Exception e) {
			rsp.onFailure(e.getMessage());
			return null;
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (conn != null) {
					conn.disconnect();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static String getString(ArrayList<String> list) {
		if (list == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (String s : list) {
			sb.append(s);
		}
		return sb.toString();
	}
}
