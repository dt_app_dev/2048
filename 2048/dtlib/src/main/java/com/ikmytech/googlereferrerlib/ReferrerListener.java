package com.ikmytech.googlereferrerlib;

/**
 * Created by shaobo on 2017/12/26.
 */

public interface ReferrerListener {
    public void onReferrerLoad(ReferrerInfo referrerInfo);
    public void onReferrerFail();
}
