package com.randomxtop.twozerogame;

import android.app.Application;

import com.ikmytech.analyticsdk.Tracker;

/**
 * Created by shaobo on 15/10/29.
 */
public class MainApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Tracker.initTracker(this,"UA-69446276-1");
    }
}
