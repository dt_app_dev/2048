package com.ikmytech.analyticsdk;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;

/**
 * Created by shaobo on 2018/1/8.
 */

public class Tracker {
    public static GoogleAnalytics analytics;
    public static com.google.android.gms.analytics.Tracker tracker;

    public static void initTracker(Context context, String trackerId){
        analytics = GoogleAnalytics.getInstance(context);
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker(trackerId); // Replace with actual tracker/property Id
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
    }
}
