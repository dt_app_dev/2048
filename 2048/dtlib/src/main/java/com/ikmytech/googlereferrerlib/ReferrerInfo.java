package com.ikmytech.googlereferrerlib;

/**
 * Created by shaobo on 2017/12/26.
 */

public class ReferrerInfo {
    public String referrer;
    public long clickTime;
    public long installTime;
}
