package com.randomxtop.twozerogame;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ikmytech.postback.PostbackUtils;

/**
 * Created by shaobo on 2018/1/5.
 */

public class ReferrerReceiver extends BroadcastReceiver {
    private static final String APP_FIRST_INSTLL_RECEIVER = "APP_FIRST_INSTALL_RECEIVER";

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        boolean isFirst = settings.getBoolean(APP_FIRST_INSTLL_RECEIVER, true);
        if (isFirst) {
            PostbackUtils.postbackReceiver(context, intent, Constants.LOG_URL, "190");
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(APP_FIRST_INSTLL_RECEIVER, false);
            editor.apply();
        }

    }

}
