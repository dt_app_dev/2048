package com.ikmytech.postback;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.INTERNET;

public class DeviceInfoUtils {
    private static SimpleDateFormat dateFormat;
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'Z";
    private static TelephonyManager mTelephonyManager = null;


    private DeviceInfoUtils(Context context) {

    }

    public static String getUdid(final Context context) {
        if (context == null) {
            return null;
        }

        String udid = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return udid;
    }


    public static boolean isNetworkAvailable(final Context context) {
        if (context == null) {
            return false;
        }

        final int internetPermission = context
                .checkCallingOrSelfPermission(INTERNET);
        if (internetPermission == PackageManager.PERMISSION_DENIED) {
            return false;
        }

        /**
         * This is only checking if we have permission to access the network
         * state It's possible to not have permission to check network state but
         * still be able to access the network itself.
         */
        final int networkAccessPermission = context
                .checkCallingOrSelfPermission(ACCESS_NETWORK_STATE);
        if (networkAccessPermission == PackageManager.PERMISSION_DENIED) {
            return true;
        }

        // Otherwise, perform the connectivity check.
        try {
            final ConnectivityManager connnectionManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo networkInfo = connnectionManager
                    .getActiveNetworkInfo();
            return networkInfo.isConnected();
        } catch (NullPointerException e) {
            return false;
        }
    }


    private static String getPackageName(final Context context) {
        final String packageName = context.getPackageName();
        return sanitizeString(packageName, "");
    }


    public static String sanitizeStringShort(final String string) {
        return sanitizeString(string, "");
    }

    private static String sanitizeString(final String string,
                                         final String defaultString) {
        String result = string;
        if (TextUtils.isEmpty(result)) {
            result = defaultString;
        }

        result = result.replaceAll("\\s", "");
        if (TextUtils.isEmpty(result)) {
            result = defaultString;
        }

        return result;
    }

    public static String quote(String string) {
        if (string == null) {
            return null;
        }

        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher = pattern.matcher(string);
        if (!matcher.find()) {
            return string;
        }

        return String.format("'%s'", string);
    }

    public static String dateFormat(long date) {
        if (null == dateFormat) {
            dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.US);
        }
        return dateFormat.format(date);
    }

    public static JSONObject buildJsonObject(String jsonString) {
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
        }

        return jsonObject;
    }

    public static Bundle getApplicationBundle(Context context) {
        final ApplicationInfo applicationInfo;
        try {
            String packageName = context.getPackageName();
            applicationInfo = context.getPackageManager().getApplicationInfo(
                    packageName, PackageManager.GET_META_DATA);
            return applicationInfo.metaData;
        } catch (NameNotFoundException e) {

        } catch (Exception e) {

        }
        return null;
    }

    public static String getDeviceInfoParamsWithParams(Context mContext,
                                                       Map<String, String> params) {
        StringBuilder mStringBuilder = new StringBuilder();
        if (params != null) {
            for (String key : params.keySet()) {
                addParam(mStringBuilder, key, params.get(key));
            }
        }
        // device info
        addParam(mStringBuilder, "udid", getUdid(mContext));
        // device build
        addParam(mStringBuilder, "osv", String.valueOf(Build.VERSION.SDK));
        addParam(mStringBuilder, "dmf", Build.MANUFACTURER);
        addParam(mStringBuilder, "dml", Build.MODEL);
        addParam(mStringBuilder, "dpd", Build.PRODUCT);
        // orientation
        int orientation = mContext.getResources().getConfiguration().orientation;
        addParam(mStringBuilder, "so", String.valueOf(orientation));
        // density
        float density = mContext.getResources().getDisplayMetrics().density;
        addParam(mStringBuilder, "ds", String.valueOf(density));
        // mcc, mnc
        // iso country code
        if (mTelephonyManager == null) {
            mTelephonyManager = (TelephonyManager) mContext
                    .getSystemService(Context.TELEPHONY_SERVICE);
        }
        String isoCountryCode = mTelephonyManager.getNetworkCountryIso();
        addParam(mStringBuilder, "icc", isoCountryCode);
        // carrier name
        String carrierName = mTelephonyManager.getNetworkOperatorName();
        addParam(mStringBuilder, "cn", carrierName);
        addParam(mStringBuilder, "pk", getPackageName(mContext));

        return mStringBuilder.toString();
    }

    public static String getDeviceInfoParams(Context mContext) {
        return getDeviceInfoParamsWithParams(mContext, null);
    }

    public static void addParam(StringBuilder mStringBuilder, String key,
                                String value) {
        if (null == key || 0 == key.length() || null == value) {
            return;
        }

        mStringBuilder.append(key);
        mStringBuilder.append("=");
        mStringBuilder.append(Uri.encode(value));
        mStringBuilder.append("&");
    }

    public static String getInstallInfo(Context context) {
        String installSource = "";
        try {
            String packageName = getPackageName(context);
            PackageManager packageManager = context.getPackageManager();
            installSource = packageManager.getInstallerPackageName(packageName);
        } catch (Exception e) {

        }
        return installSource;
    }


    public static String getInstallTime(Context context) {
        long firstInstallTime = 0;
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            firstInstallTime = packageInfo.firstInstallTime;
            // }
        } catch (Exception e) {
        }
        return String.valueOf(firstInstallTime);
    }
}
