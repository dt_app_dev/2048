package com.ikmytech.postback;

import android.content.Context;
import android.util.Log;

import com.ikmytech.postback.executor.DefaultExecutorSupplier;

import java.util.ArrayList;
import java.util.Map;

public class LogUtils {

    public static void sendEventToLogUrlInBack(final Context mContext,
                                               final String logUrl, boolean useDefault,
                                               final Map<String, String> params) {

        String localUrl = logUrl;
        if (useDefault) {
            String deviceInfos = DeviceInfoUtils.getDeviceInfoParamsWithParams(
                    mContext, params);
            if (logUrl.indexOf("?") > 0) {
                localUrl = logUrl + "&" + deviceInfos;
            } else {
                localUrl = logUrl + "?" + deviceInfos;
            }
        }
        if (!DeviceInfoUtils.isNetworkAvailable(mContext)) {
            Log.i("sendEventToLogUrl",
                    "Network not available,cancel sendEventToTrackUrl");
            return;
        }
        try {
            HttpUtil.http_get(localUrl, new IHttpResponse() {
                @Override
                public void onResponse(ArrayList<String> result) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onFailure(String errorMessage) {
                    // TODO Auto-generated method stub

                }
            });
        } catch (Exception exception) {
            Log.w("sendEventToLogUrl", "Error executing sendEventToTrackUrl",
                    exception);
        }
    }

    public static void sendEventToLogUrl(final Context mContext,
                                         final String logUrl, final Map<String, String> params) {

        LogUtils.sendEventToLogUrl(mContext, logUrl, true, params);
    }

    public static void sendEventToLogUrl(final Context mContext,
                                         final String logUrl, final boolean useDefault,
                                         final Map<String, String> params) {

        try {
            DefaultExecutorSupplier.getInstance()
                    .forLightWeightBackgroundTasks().execute(new Runnable() {

                @Override
                public void run() {
                    sendEventToLogUrlInBack(mContext, logUrl,
                            useDefault, params);
                }
            });
        } catch (Exception e) {

        }
    }
}
