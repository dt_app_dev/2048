package com.ikmytech.googlereferrerlib;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.ikmytech.postback.LogUtils;

import java.util.HashMap;

/**
 * Created by shaobo on 2017/12/26.
 */

public class ReferrerManager {
    private Context mContext;
    private static ReferrerManager referrerManager = null;

    private ReferrerManager(Context context){
        mContext = context;
    }

    public static synchronized ReferrerManager getManager(Context context){
        if (referrerManager == null) {
            referrerManager = new ReferrerManager(context);
        }
        return referrerManager;
    }

    public void getIntallReferrer(final ReferrerListener referrerListener) {
        final InstallReferrerClient mReferrerClient = InstallReferrerClient.newBuilder(mContext).build();
        mReferrerClient.startConnection(new InstallReferrerStateListener() {
            public void onInstallReferrerSetupFinished(int responseCode) {
                switch (responseCode) {
                    case InstallReferrerClient.InstallReferrerResponse.OK:
                        ReferrerDetails response = null;
                        try {
                            response = mReferrerClient.getInstallReferrer();
                            String referrer =  response.getInstallReferrer();
                            long cttime = response.getReferrerClickTimestampSeconds();
                            long ittime = response.getInstallBeginTimestampSeconds();
                            if (referrerListener != null) {
                                ReferrerInfo referrerInfo = new ReferrerInfo();
                                referrerInfo.referrer = referrer;
                                referrerInfo.clickTime = cttime;
                                referrerInfo.installTime = ittime;
                                referrerListener.onReferrerLoad(referrerInfo);
                            }
                        } catch (RemoteException e) {
                            if (referrerListener != null) {
                                referrerListener.onReferrerFail();
                            }
                        }

                        break;
                    case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                        if (referrerListener != null) {
                            referrerListener.onReferrerFail();
                        }
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                        if (referrerListener != null) {
                            referrerListener.onReferrerFail();
                        }
                        break;
                }
            }

            public void onInstallReferrerServiceDisconnected() {
                Log.e("mReferrerClient","onInstallReferrerServiceDisconnected");
            }
        });

    }
}
