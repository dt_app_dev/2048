package com.ikmytech.postback;

import java.util.ArrayList;

public interface IHttpResponse {
	public void onResponse(ArrayList<String> result);

	public void onFailure(String errorMessage);
}
