package com.ikmytech.postback;

import android.content.Context;
import android.content.Intent;

import com.ikmytech.googlereferrerlib.ReferrerInfo;
import com.ikmytech.googlereferrerlib.ReferrerListener;
import com.ikmytech.googlereferrerlib.ReferrerManager;

import java.util.HashMap;

/**
 * Created by shaobo on 2018/1/3.
 */

public class PostbackUtils {

    public static void postback(final Context mContext, final String postUrl, final String logUrl, final String type) {
        ReferrerManager referrerManager = ReferrerManager.getManager(mContext);
        if (referrerManager != null) {
            referrerManager.getIntallReferrer(new ReferrerListener() {
                @Override
                public void onReferrerLoad(ReferrerInfo referrerInfo) {
                    String referrer = referrerInfo.referrer;
                    long clickTime = referrerInfo.clickTime;
                    long installTime = referrerInfo.installTime;
                    if (clickTime > 0 && installTime > 0 && clickTime < installTime) {
                        HashMap<String, String> mapRequest = new HashMap<>();
                        String[] arrSplit = referrer.split("[&]");
                        for (String strSplit : arrSplit) {
                            String[] arrSplitEqual = strSplit.split("[=]");
                            if (arrSplitEqual.length > 1) {
                                mapRequest.put(arrSplitEqual[0], arrSplitEqual[1]);
                            }
                        }
                        mapRequest.put("ctime", String.valueOf(clickTime));
                        mapRequest.put("itime", String.valueOf(installTime));
                        LogUtils.sendEventToLogUrl(mContext, postUrl, mapRequest);
                    }
                    HashMap<String, String> params = new HashMap<>();
                    params.put("referrer", referrer);
                    params.put("ctime", String.valueOf(clickTime));
                    params.put("itime", String.valueOf(installTime));
                    params.put("type", type);
                    params.put("aid", "1098");
                    LogUtils.sendEventToLogUrl(mContext, logUrl, params);
                }

                @Override
                public void onReferrerFail() {

                }
            });
        }
    }

    public static void postbackReceiver(Context context, Intent intent, String postUrl , String type){
        String REFERRER = "referrer";
        String rawReferrer = intent.getStringExtra(REFERRER);

        if (null == rawReferrer) {
            return;
        }

        String installSource = DeviceInfoUtils.getInstallInfo(context);
        String installTime = DeviceInfoUtils.getInstallTime(context);
        HashMap<String, String> params = new HashMap<>();
        params.put("referrer", rawReferrer);
        params.put("type", type);
        params.put("aid", "1098");
        params.put("insrc", installSource);
        params.put("itime", installTime);
        LogUtils.sendEventToLogUrl(context, postUrl, params);
    }
}
